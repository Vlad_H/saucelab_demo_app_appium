import type { Options } from '@wdio/types'
export const config: Options.Testrunner = {
    runner: 'local',
    autoCompileOpts: {
        autoCompile: true,
        tsNodeOpts: {
            project: './tsconfig.json',
            transpileOnly: true
        }
    },
    port: process.env.APPIUM_PORT,
    specs: [
        './test/specs/**/*.ts'
    ],
    // Patterns to exclude.
    exclude: [
        // 'path/to/excluded/files'
    ],
    maxInstances: 10,
    capabilities: [{
        platformName: process.env.PLATFORM_NAME,
        'appium:automationName': process.env.AUTOMATION_NAME,
        'appium:deviceName': process.env.DEVICE_NAME,
        'appium:app': process.env.APP_PATH,
    }],
    logLevel: 'info',
    bail: 0,
    baseUrl: '',
    waitforTimeout: 10000,
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,
    services: ['appium'],
    framework: 'mocha',
    reporters: [
        ['spec', {
        symbols: {
            passed: '[PASS]',
            failed: '[FAIL]',
        },
    }],
    ],
    mochaOpts: {
        ui: 'bdd',
        timeout: 60000
    },
}
