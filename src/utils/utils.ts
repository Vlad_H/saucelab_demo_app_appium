export function replaceDollarSign(elem: string): number {
    return +elem.replace(/\$/g, '');
}

export function checkSortByPriceDesc(arr: string[]): boolean {
    const temp  = arr.map((item)=>{
        return replaceDollarSign(item)
    })
    const reversed = temp.sort((a,b)=> {return b  - a})
    return temp === reversed
}

export function checkSortByPriceAsc(arr: string[]): boolean {
    const temp  = arr.map((item)=>{
        return replaceDollarSign(item)
    })
    const reversed = temp.sort((a,b)=> {return a  - b})
    return temp === reversed
}

export function checkSortByNameAsc(arr: string[]): boolean {
    return arr === arr.sort((a,b)=>{return a.localeCompare(b)})
}

export function checkSortByNameDesc(arr: string[]): boolean {
    return arr === arr.sort((a,b)=>{return b.localeCompare(a)})
}
