
# Prerequisites
1. Install Node 18 version

1. Install JDK and set up the "JAVA_HOME" path using commands:

```
export JAVA_HOME="path that you found"
export PATH=$JAVA_HOME/bin:$PATH
```
2. Install Android Studio and set up the "ANDROID_HOME" path using commands:
```
export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/tools
```
3. Install Appium globally:
```
npm install -g appium@next
```
Check appium version:
```
appium -v
```
4. (OPTIONAL) Install Appium Doctor globally:
```
npm install appium-doctor -g
```
You can check, if your OS meets the appium requirements by executing the command:
```
appium-doctor
```
5. Install UIAutomator2 Android driver:
```
appium driver install uiautomator2
```
Check the installed driver:
```
appium driver list
```
6. Download the latest version of Sauce Labs Demo App (https://github.com/saucelabs/my-demo-app-rn/releases)

7. Run the emulator by creating a virtual device in Android Studio and running it (instructions: https://developer.android.com/studio/run/managing-avds)

8. Run Appium server:
```
appium
```

7. Create .env file in the root of the project and create such capabilities:
```
APPIUM_HOST={HOST_NAME}
APPIUM_PORT={PORT_NUMBER}
PLATFORM_NAME=Android
AUTOMATION_NAME=uiautomator2
DEVICE_NAME={YOUR_EMULATOR_ID}
APP_PATH={PATH_TO_THE_INSTALLED_APP}
```
8. Install node modules:
```
yarn
```
OR:
```
npm i
```

## Running Tests

To run single spec, run the following command

```
yarn run wdio --spec <path_to_the_spec_file> 
Example: yarn run wdio --spec ./test/specs/login.e2e.ts
```
OR:
```
npm run wdio --spec <path_to_the_spec_file> 
Example: npm run wdio --spec ./test/specs/login.e2e.ts

