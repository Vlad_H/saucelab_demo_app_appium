export const data  = {
  VALID_LOGIN_CREDENTIALS: {
    login: "bob@example.com",
    password: "10203040"
  },
  INVALID_USERNAME_CREDENTIALS: {
    login: "test@example.com",
    password: "10203040"
  },
  INVALID_PASSWORD_CREDENTIALS: {
    login: "bob@example.com",
    password: "123456789"
  },
  INVALID_CREDENTIALS: {
    login: "test@example.com",
    password: "123456789"
  }
}