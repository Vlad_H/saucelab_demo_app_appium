import BurgerMenuPage from "../pageobjects/burgerMenu.page.ts";
import LoginPage from "../pageobjects/login.page.ts";
import {data} from "../testing_data/data.ts"
import HeaderPage from "../pageobjects/header.page.ts";

describe('Login page', () => {

    before(async () => {
        await HeaderPage.burgerMenu.waitForDisplayed()
        await HeaderPage.burgerMenu.click()
        await BurgerMenuPage.loginOption.click()
    })


    it('should show `Username is required` error', async () => {
        await LoginPage.loginBtn.click()
        await expect(LoginPage.usernameRequiredError).toBeDisplayed()
    });

    it('should show `Password is required` error', async () => {
        await LoginPage.loginInput.setValue(data.VALID_LOGIN_CREDENTIALS.login)
        await LoginPage.loginBtn.click()
        await expect(LoginPage.passwordRequiredError).toBeDisplayed()
    });

    it('should show `Invalid credentials` error with valid login and invalid password', async () => {
        await LoginPage.login(data.INVALID_PASSWORD_CREDENTIALS.login, data.INVALID_PASSWORD_CREDENTIALS.password)
        await expect(LoginPage.invalidCredentialsError).toBeDisplayed()
    });

    it('should show `Invalid credentials` error with invalid login and valid password', async () => {
        await LoginPage.login(data.INVALID_USERNAME_CREDENTIALS.login, data.INVALID_USERNAME_CREDENTIALS.password)
        await expect(LoginPage.invalidCredentialsError).toBeDisplayed()
    });

    it('should show `Invalid credentials` error with invalid login and invalid password', async () => {
        await LoginPage.login(data.INVALID_CREDENTIALS.login, data.INVALID_CREDENTIALS.password)
        await expect(LoginPage.invalidCredentialsError).toBeDisplayed()
    });

    it('should login user with valid credentials', async () => {
        await LoginPage.login(data.VALID_LOGIN_CREDENTIALS.login, data.VALID_LOGIN_CREDENTIALS.password)
    });
})

