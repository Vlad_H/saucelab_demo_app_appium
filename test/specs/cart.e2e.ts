import MainPage from "../pageobjects/main.page.ts";
import ProductPage from "../pageobjects/product.page.ts";
import HeaderPage from "../pageobjects/header.page.ts";
import CartPage from "../pageobjects/cart.page.ts";
import BurgerMenuPage from "../pageobjects/burgerMenu.page.ts";
import {replaceDollarSign} from "../../src/utils/utils.ts";

describe('Cart page', () => {

    let firstProduct = {
        price: 0,
        count: 0
    }

    let secondProduct = {
        price: 0,
        count: 0
    }

    let totalCount = 0;
    let totalPrice = 0;

    it('should show `No Cart Items` when there are no products in the cart', async () => {
        await HeaderPage.cartBtn.click()
        expect(await CartPage.noItemsSpan).toBeDisplayed()
    });

    it('should return user to the main page by clicking on the `Go Shopping` button', async () => {
        await CartPage.goShoppingBtn.click()
        expect(await MainPage.storeItems[0]).toBeClickable()
    });

    it('should add the first product to the cart', async () => {
        await MainPage.storeItems[0].click()
        await ProductPage.productCounterPlusBtn.doubleClick()
        firstProduct.price = replaceDollarSign(await ProductPage.productPrice.getText())
        firstProduct.count = +await ProductPage.productCounter.getText()
        await ProductPage.addToCartBtn.click()
        expect(await HeaderPage.cartIconCounter.getText()).toBe('3')
    });

    it('should add the second product to the cart', async () => {
        await HeaderPage.burgerMenu.click()
        await BurgerMenuPage.catalogOption.click()
        await browser.pause(500)
        await MainPage.storeItems[1].waitForDisplayed()
        await MainPage.storeItems[1].click()
        await ProductPage.productCounterPlusBtn.click()
        secondProduct.price = replaceDollarSign(await ProductPage.productPrice.getText())
        secondProduct.count = +await ProductPage.productCounter.getText()
        await ProductPage.addToCartBtn.click()
        await browser.pause(500)
        expect(await HeaderPage.cartIconCounter.getText()).toBe('5')
    });

    it('should show the added items in the cart', async () => {
        await HeaderPage.cartBtn.click()
        await CartPage.checkoutBtn.waitForEnabled()
        expect(await CartPage.cartItems.length).toEqual(2)
    });

    it('should correctly display the total amount of products and total price in the cart', async () => {
        totalCount = firstProduct.count + secondProduct.count
        console.log(`First Total Count: ${totalCount}`)
        expect(await CartPage.totalCartAmount.getText()).toBe(`${totalCount} items`)
        totalPrice = firstProduct.price * firstProduct.count + secondProduct.price * secondProduct.count
        expect(await CartPage.totalCartPrice.getText()).toBe(`$${totalPrice}`)
    });

    it('should increase the amount of item and correctly re-calculate total price', async () => {
        await CartPage.counterPlusButtons[0].click()
        await browser.pause(500)
        expect(await CartPage.itemCounters[0].getText()).toBe('4')
        firstProduct.count++
        totalPrice+=firstProduct.price
        expect(await CartPage.totalCartAmount.getText()).toBe(`${totalCount + 1} items`)
        expect(await CartPage.totalCartPrice.getText()).toBe(`$${totalPrice}`)
    });

    it('should decrease the amount of item and correctly re-calculate total price', async () => {
        await CartPage.counterMinusButtons[1].click()
        await browser.pause(1000)
        expect(await CartPage.itemCounters[1].getText()).toBe('1')
        secondProduct.count--
        totalPrice-=secondProduct.price
        expect(await CartPage.totalCartAmount.getText()).toBe(`${totalCount} items`)
        expect(await CartPage.totalCartPrice.getText()).toBe(`$${totalPrice}`)
    });

    it('should remove item from cart and correctly re-calculate total price', async () => {
        await CartPage.cartItemsRemoveBtn[0].click()
        await browser.pause(500)
        expect(await CartPage.totalCartAmount.getText()).toBe(`${totalCount - firstProduct.count} item`)
        expect(await CartPage.totalCartPrice.getText()).toBe(`$${(totalPrice - (firstProduct.price * firstProduct.count)).toFixed(2)}`)
    });

    it('should redirect to the main page by clicking on `Proceed to checkout` button', async () => {
        await CartPage.checkoutBtn.click()
        expect(await MainPage.storeItems[0]).toBeDisplayed()
    });


})

