import MainPage from "../pageobjects/main.page.ts";
import ProductPage from "../pageobjects/product.page.ts";
import HeaderPage from "../pageobjects/header.page.ts";
import CartPage from "../pageobjects/cart.page.ts";

describe('Product page', () => {


    it('should open the right product', async () => {
        const productTitle = await MainPage.storeItemsTitle[0].getText()
        const productPrice = await MainPage.storeItemsPrice[0].getText()
        await MainPage.storeItems[0].click()
        expect(await ProductPage.productTitle.getText()).toBe(productTitle)
        expect(await ProductPage.productPrice.getText()).toBe(productPrice)
    });


    it('should add the product', async () => {
        await ProductPage.fourStarOption.click()
        await ProductPage.checkReviewModalToBeVisible()
        await ProductPage.reviewModalCloseBtn.click()
    });

    it('should increase and decrease the product counter', async () => {
        await ProductPage.productCounterPlusBtn.click()
        await ProductPage.productCounterPlusBtn.click()
        expect(await ProductPage.productCounter.getText()).toBe('3')
        await ProductPage.productCounterMinusBtn.click()
        expect(await ProductPage.productCounter.getText()).toBe('2')
    });


    it('should add the product to the cart with a chosen color', async () => {
        await ProductPage.blueColorOption.click()
        await ProductPage.addToCartBtn.click()
        expect(await HeaderPage.cartIconCounter).toBeDisplayed()
        expect(await HeaderPage.cartIconCounter.getText()).toBe('2')
        await HeaderPage.cartBtn.click()
        expect(await CartPage.blueColorOption).toBeDisplayed()
    });


})

