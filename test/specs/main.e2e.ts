import MainPage from "../pageobjects/main.page.ts";
import HeaderPage from "../pageobjects/header.page.ts";
import SortButtonPage from "../pageobjects/sortButton.page.ts";
import {
    checkSortByNameAsc,
    checkSortByNameDesc,
    checkSortByPriceAsc,
    checkSortByPriceDesc
} from "../../src/utils/utils.ts";

describe('Main page', () => {

    it('should sort items by price in ascending order', async () => {
        await HeaderPage.sortProductsBtn.click()
        await SortButtonPage.priceAscendingOption.click()
        await browser.pause(1000)
        const prices = await MainPage.storeItemsPrice.map((item) => {return item.getText()})
        await expect(checkSortByPriceAsc(prices)).toBeTruthy()
    });

    it('should sort items by price in descending order', async () => {
        await HeaderPage.sortProductsBtn.click()
        await SortButtonPage.priceDescendingOption.click()
        await browser.pause(1000)
        const prices = await MainPage.storeItemsPrice.map((item) => {return item.getText()})
        await expect(checkSortByPriceDesc(prices)).toBeTruthy()
    });

    it('should sort items by name in ascending order', async () => {
        await HeaderPage.sortProductsBtn.click()
        await SortButtonPage.nameAscendingOption.click()
        await browser.pause(1000)
        const prices = await MainPage.storeItemsTitle.map((item) => {return item.getText()})
        await expect(checkSortByNameAsc(prices)).toBeTruthy()
    });

    it('should sort items by name in descending order', async () => {
        await HeaderPage.sortProductsBtn.click()
        await SortButtonPage.nameDescendingOption.click()
        await browser.pause(1000)
        const prices = await MainPage.storeItemsTitle.map((item) => {return item.getText()})
        await expect(checkSortByNameDesc(prices)).toBeTruthy()
    });


})

