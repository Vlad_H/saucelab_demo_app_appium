import { $ } from '@wdio/globals'


export class ProductPage {

    public get addToCartBtn() {
        return $('//android.view.ViewGroup[@content-desc="Add To Cart button"]')
    }

    public get productPrice() {
        return $('//android.widget.TextView[@content-desc="product price"]')
    }

    public get productCounter() {
        return $('//android.view.ViewGroup[@content-desc="counter amount"]').$('//android.widget.TextView')
    }

    public get productCounterPlusBtn() {
        return $('//android.view.ViewGroup[@content-desc="counter plus button"]')
    }

    public get productCounterMinusBtn() {
        return $('//android.view.ViewGroup[@content-desc="counter minus button"]')
    }

    public get blueColorOption() {
        return $('//android.view.ViewGroup[@content-desc="blue circle"]')
    }

    public get grayColorOption() {
        return $('//android.view.ViewGroup[@content-desc="gray circle"]')
    }

    public get redColorOption() {
        return $('//android.view.ViewGroup[@content-desc="red circle"]')
    }

    public get blackColorOption() {
        return $('//android.view.ViewGroup[@content-desc="black circle"]')
    }

    public get productDescription() {
        return $('//android.widget.TextView[@content-desc="product description"]')
    }

    public get oneStarOption() {
        return $('//android.view.ViewGroup[@content-desc="review star 1"]')
    }

    public get twoStarOption() {
        return $('//android.view.ViewGroup[@content-desc="review star 2"]')
    }

    public get threeStarOption() {
        return $('//android.view.ViewGroup[@content-desc="review star 3"]')
    }

    public get fourStarOption() {
        return $('//android.view.ViewGroup[@content-desc="review star 4"]')
    }

    public get fiveStarOption() {
        return $('//android.view.ViewGroup[@content-desc="review star 5"]')
    }

    public get reviewModalSpan() {
        return $('//android.widget.TextView[@text="Thank you for submitting your review!"]')
    }

    public get reviewModalCloseBtn() {
        return $('//android.view.ViewGroup[@content-desc="Close Modal button"]')
    }

    public get productTitle() {
        return $('//android.view.ViewGroup[@content-desc="container header"]').$('//android.widget.TextView')
    }

    public async checkReviewModalToBeVisible() {
        expect(await this.reviewModalSpan).toBeDisplayed()
        expect(await this.reviewModalCloseBtn).toBeClickable()
    }


}

export default new ProductPage()
