import { $, $$ } from '@wdio/globals'


export class CartPage {

    public get goShoppingBtn() {
        return $('//android.view.ViewGroup[@content-desc="Go Shopping button"]')
    }

    public get noItemsSpan() {
        return $('//android.widget.TextView[@text="No Items"]')
    }

    public get cartItems() {
        return $$('//android.view.ViewGroup[@content-desc="product row"]')
    }

    public get cartItemsRemoveBtn() {
        return $$('//android.view.ViewGroup[@content-desc="remove item"]')
    }

    public get blueColorOption() {
        return $('//android.view.ViewGroup[@content-desc="blue circle"]')
    }

    public get grayColorOption() {
        return $('//android.view.ViewGroup[@content-desc="gray circle"]')
    }

    public get redColorOption() {
        return $('//android.view.ViewGroup[@content-desc="red circle"]')
    }

    public get blackColorOption() {
        return $('//android.view.ViewGroup[@content-desc="black circle"]')
    }

    public get totalCartAmount() {
        return $('//android.widget.TextView[@content-desc="total number"]')
    }

    public get totalCartPrice() {
        return $('//android.widget.TextView[@content-desc="total price"]')
    }

    public get checkoutBtn() {
        return $('//android.view.ViewGroup[@content-desc="Proceed To Checkout button"]')
    }

    public get counterPlusButtons() {
        return $$('//android.view.ViewGroup[@content-desc="counter plus button"]')
    }

    public get counterMinusButtons() {
        return $$('//android.view.ViewGroup[@content-desc="counter minus button"]')
    }

    public get itemCounters() {
        return $$('//android.view.ViewGroup[@content-desc="counter amount"]/android.widget.TextView')
    }

}

export default new CartPage()
