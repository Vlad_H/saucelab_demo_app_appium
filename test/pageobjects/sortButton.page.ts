import { $ } from '@wdio/globals'

class SortButtonPage {
    public get nameAscendingOption() {
        return $('//android.view.ViewGroup[@content-desc="nameAsc"]')
    }

    public get nameDescendingOption() {
        return $('//android.view.ViewGroup[@content-desc="nameDesc"]')
    }

    public get priceAscendingOption() {
        return $('//android.view.ViewGroup[@content-desc="priceAsc"]')
    }

    public get priceDescendingOption() {
        return $('//android.view.ViewGroup[@content-desc="priceDesc"]')
    }


}

export default new SortButtonPage()