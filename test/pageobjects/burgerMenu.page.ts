import { $ } from '@wdio/globals'

class BurgerMenuPage {

    public get catalogOption() {
        return $('//android.view.ViewGroup[@content-desc="menu item catalog"]')
    }
    public get loginOption() {
        return $('//android.view.ViewGroup[@content-desc="menu item log in"]')
    }

    public get logoutOption() {
        return $('//android.view.ViewGroup[@content-desc="menu item log out"]')
    }
}

export default new BurgerMenuPage()