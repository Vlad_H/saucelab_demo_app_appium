import { $ } from '@wdio/globals'


export class LoginPage {

    public get loginInput() {
        return $('//android.widget.EditText[@content-desc="Username input field"]')
    }

    public get passwordInput() {
        return $('//android.widget.EditText[@content-desc="Password input field"]')
    }

    public get loginBtn() {
        return $('//android.view.ViewGroup[@content-desc="Login button"]')
    }

    public get usernameRequiredError() {
        return $('//android.widget.TextView[@text="Username is required"]')
    }

    public get passwordRequiredError() {
        return $('//android.widget.TextView[@text="Password is required"]')
    }

    public get invalidCredentialsError() {
        return $('//android.widget.TextView[@text="Provided credentials do not match any user in this service."]')
    }

    async login(email: string, password: string) {
        await this.loginInput.setValue(email)
        await this.passwordInput.setValue(password)
        await this.loginBtn.click()
    }
}

export default new LoginPage()
