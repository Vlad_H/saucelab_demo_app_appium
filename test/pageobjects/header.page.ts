import { $ } from '@wdio/globals'

class HeaderPage {
    public get burgerMenu() {
        return $('//android.view.ViewGroup[@content-desc="open menu"]')
    }

    public get sortProductsBtn() {
        return $('//android.view.ViewGroup[@content-desc="sort button"]')
    }

    public get cartBtn() {
        return $('//android.view.ViewGroup[@content-desc="cart badge"]')
    }

    public get cartIconCounter() {
        return this.cartBtn.$('//android.widget.TextView')
    }

}

export default new HeaderPage()