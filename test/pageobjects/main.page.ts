import { $, $$ } from '@wdio/globals'

class MainPage {
    public get storeItems() {
        return $$('//android.view.ViewGroup[@content-desc="store item"]')
    }

    public get storeItemsTitle() {
        return $$('//android.widget.TextView[@content-desc="store item text"]')
    }

    public get storeItemsPrice() {
        return $$('//android.widget.TextView[@content-desc="store item price"]')
    }

}

export default new MainPage()